package com.ivaylo.mohamed.projettp1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import android.widget.NumberPicker
import android.widget.NumberPicker.OnValueChangeListener




class MainActivity : AppCompatActivity(), AnkoLogger {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Number picker
        numberPicker.minValue = 0
        numberPicker.maxValue = 10

        // Affichage message d'information. Question D2
        info("Message information onCreate")

        // Ajout d'un listener sur le bouton.
        changeTextBtn.setOnClickListener{
            // On change le texte du label initial. Question C5
            initialText.text = "Ce texte a été changer grace au bouton."
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        
        if (outState != null) {
            outState.putInt("tmpNumberPickerValue", numberPicker.value)
        }
    }


    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)

        if (savedInstanceState != null) {
            numberPicker.value = savedInstanceState.getInt("tmpNumberPickerValue")
        }
    }

    override fun onStart() {
        super.onStart()
        info("Message information onStart")
    }

    override fun onResume() {
        super.onResume()
        info("Message information onResume")
    }

    override fun onRestart() {
        super.onRestart()
        info("Message information onRestart")
    }

    override fun onPause() {
        super.onPause()
        info("Message information onPause")
    }

    override fun onStop() {
        super.onStop()
        info("Message information onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        info("Message information onDestroy")
    }
}
